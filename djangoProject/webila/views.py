from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return render(request, 'story03.html')

def about(request):
    context = {"about_page" : "active"}
    return render(request, 'aboutme.html', context)

def experiences(request):
    context = {"experiences_page" : "active"}
    return render(request, 'Experiences.html', context)

def skills(request):
    context = {"skills_page" : "active"}
    return render(request, 'Skills.html', context)

def contacts(request):
    context = {"contacts_page" : "active"}
    return render(request, 'contacts.html', context)

def chibird(request):
    context = {"chibird_page" : "active"}
    return render(request, 'chibird.html', context)

# Create your views here.

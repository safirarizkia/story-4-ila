from django.apps import AppConfig


class WebilaConfig(AppConfig):
    name = 'webila'
